# mybatis-plus-reverse

#### 介绍
mybatis-plus逆向工具
![输入图片说明](https://foruda.gitee.com/images/1660444625894967117/1.png "1.png")

#### 软件架构
这个命令行工具主要分为下面几个部分：

- 命令行参数的解析
- 配置文件解析并读取相关的表和表的结构
- 生成实体、`Mapper`和`xml`文件，（这里考虑到实现简单并且尽量减少依赖使用并没有使用模板引擎进行实现）

整个项目使用一些基础的依赖包

```toml
[dependencies]
# json解析
serde_json = "1.0"  
serde = { version = "1.0", features = ["derive"] }
# 命令行颜色
colored = "2.0.0"
# 时间格式化
chrono = "0.4.19"
# 数据库连接
mysql = "22.1.0"
```

接着看一下工具的配置文件格式（现在支持`Json`格式）

```json
{
  "package_path": "com.example.demo",  # 项目主包
  "pass_print": false,
  "database": {   # 数据库配置
    "address": "127.0.0.1",
    "port": 3306,
    "username": "root",
    "password": "123456",
    "database": "test",
    "is_all": false,  # 是否逆向完整的数据库
    "table_name": ["t_user", "t_address"],  # 逆向指定的表
    "table_prefix": "t_"  # 表前缀
  },
  "entity": {  # 实体配置
    "primary_key_type": "AUTO",  # 主键生成策略
    "entity_path": "com.example.demo.entity",  # 实体包路径
    "use_lombok": true,
    "time_format": "2022-08-3 23:22:11",
    "time_zone": "GM+8"
  }
}
```


#### 使用说明

执行命令：`mp-reverse -a demo -p F:\temp -f F:\temp\config.json`
![输入图片说明](https://foruda.gitee.com/images/1660444657396858605/2.png "2.png")
接着我们在指定目录中查看

```
│  config.json            # 配置文件
│
├─Order                  
│      Order.java         # 实体  
│      OrderMapper.java   # mapper文件
│      OrderMapper.xml    # xml文件
│
└─Product
        Product.java
        ProductMapper.java
        ProductMapper.xml
```

展示一下其中xml文件吧

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE builder PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-builder.dtd">
<mapper namespace="com.example.demo.mapper.OrderMapper">
	<!-- 通用查询映射结果 -->
	<resultMap id="BaseResultMap" type="com.example.demo.entity.Order">
		<id column="id" property="id" />
		<result column="order_num" property="orderNum" />
		<result column="order_title" property="orderTitle" />
		<result column="merchant_id" property="merchantId" />
		<result column="order_type" property="orderType" />
		<result column="product_id" property="productId" />
	</resultMap>
	<!-- 通用查询结果列 -->
	<sql id="Base_Column_List">
		id,order_num,order_title,merchant_id,order_type,product_id
	</sql>
</mapper>
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
