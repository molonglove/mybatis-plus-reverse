/**
char/varchar/text/tinytext/mediumtext/longtext => String
tinyblob/blob/mediumblob/longblob => byte[]
tinyint/smallint/int => Integer
float => Float
double => Double
date => LocalDate => java.time.LocalDate
time => LocalTime => java.time.LocalTime
timestamp/datetime => LocalDateTime => java.time.LocalDateTime
year => Date => java.sql.Date
decimal => BigDecimal => java.math.BigDecimal
 */


pub enum JavaType {
    Bytes,
    Str,
    Integer,
    Long,
    Float,
    Double,
    Boolean,
    BigDecimal,
    LocalDateTime,
    LocalDate,
    LocalTime,
    Date,
    Object
}

pub fn java_import(types: JavaType) -> Option<String> {
    match types {
        JavaType::Bytes => Some(String::from("java.lang.Byte")),
        JavaType::Str => Some(String::from("java.lang.String")),
        JavaType::Integer => Some(String::from("java.lang.Integer")),
        JavaType::Long => Some(String::from("java.lang.Long")),
        JavaType::Float=> Some(String::from("java.lang.Float")),
        JavaType::Double=> Some(String::from("java.lang.Double")),
        JavaType::Boolean=> Some(String::from("java.lang.Boolean")),
        JavaType::BigDecimal=> Some(String::from("java.math.BigDecimal")),
        JavaType::LocalDateTime=> Some(String::from("java.time.LocalDateTime")),
        JavaType::LocalDate => Some(String::from("java.time.LocalDate")),
        JavaType::LocalTime => Some(String::from("java.time.LocalTime")),
        JavaType::Date => Some(String::from("java.sql.Date")),
        JavaType::Object => Some(String::from("java.lang.Object"))
    }
}

pub fn mysql_to_java(types: &str) -> Option<JavaType> {
    match types {
        "varchar" | "char" | "text" | "tinytext" | "mediumtext" | "longtext" => Some(JavaType::Str),
        "tinyblob" | "blob" | "mediumblob" | "longblob" => Some(JavaType::Bytes),
        "tinyint" | "smallint" | "mediumint" | "int" => Some(JavaType::Integer),
        "date" => Some(JavaType::LocalDate),
        "time" => Some(JavaType::LocalTime),
        "datetime" | "timestamp" => Some(JavaType::LocalDateTime),
        "year" => Some(JavaType::Date),
        "decimal" => Some(JavaType::BigDecimal),
        "float" => Some(JavaType::Float),
        "double" => Some(JavaType::Double),
        "bit" => Some(JavaType::Boolean),
        "id" | "integer" | "bigint" => Some(JavaType::Long),
        _ => Some(JavaType::Object),
    }
}

pub fn mysql_to_java_type(types: &str) -> Option<String> {
    match types {
        "varchar" | "char" | "text" => Some("String".to_string()),
        "date" | "time" | "datetime" | "timestamp" | "year" => Some("LocalDateTime".to_string()),
        "decimal" => Some("BigDecimal".to_string()),
        "float" => Some("Float".to_string()),
        "double" => Some("Double".to_string()),
        "boolean" | "tinyint" | "smallint" | "mediumint" | "int" => Some("Integer".to_string()),
        "bit" => Some("Boolean".to_string()),
        "bigint" => Some("BigInteger".to_string()),
        "id" | "integer" => Some("Long".to_string()),
        "blob" => Some("Bytes".to_string()),
        _ => Some("Object".to_string()),
    }
}

pub fn db_to_class(target: &str) -> Option<String> {
    mysql_to_java(target)
        .and_then(|data| java_import(data))
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_match() {
        java_import(JavaType::Str).map(|data| println!("{}", data));
    }

    #[test]
    fn test_to() {
        db_to_class("varchar").map(|data| println!("{}", data));
    }

}


