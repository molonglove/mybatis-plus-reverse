use colored::Colorize;
use crate::builder::Dispatch;
use crate::config::ApplicationConfig;
use crate::database::TableName;
use crate::flag::Flag;
use crate::utils::table_name_to_entity_name;

mod config;
mod transform;
mod utils;
mod builder;
mod database;
pub mod flag;

pub struct MpReverse;


fn load(config_path: &str, file_path: &str, author: &str) {
    let config = ApplicationConfig::new(
        config_path,
        file_path,
        author
    );
    let data: Vec<TableName> = config.load_fields();
    for value in data {
        Dispatch::new(value.clone(), config.clone()).dispatch();
        println!("{}", format!("| => 构建数据表:[{}]完成", value.table_name).green());
    }
}

impl MpReverse {
    pub fn new(args: Vec<String>) {
        let flag = Flag::new(args);
        flag.parse_match().map(|(config_path, file_path, author)| {
            load(config_path.as_str(), file_path.as_str(), author.as_str())
        });
    }

}



#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test() {
        let vec1 = vec![
            "".to_string(),
            "-a".to_string(),
            "molong".to_string(),
            "-f".to_string(),
            "config.json".to_string(),
            "-p".to_string(),
            "./temp".to_string()
        ];
        MpReverse::new(vec1);
    }

    #[test]
    fn test_1() {
        let a = String::from("PRI");
        let x = a.eq("PRI");
        println!("{}", x);
        assert_eq!(x, true);
    }

}