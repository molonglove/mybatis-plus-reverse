use std::env;
use mybatis_reverse::MpReverse;

// --config config.json
fn main() {
    MpReverse::new(env::args().collect());
}
