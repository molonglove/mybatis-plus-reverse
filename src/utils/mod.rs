// 下划线转驼峰
pub fn underline_to_hump(target: String, head_uppercase: bool) -> String {
    if !target.contains('_') && !head_uppercase {
        return target;
    }
    let mut result: Vec<u8> = Vec::new();
    let mut i = -1;
    let mut j = 0;
    for c in target.bytes() {
        if c == 95 {
            i = j + 1;
        } else {
            if i == j {
                if c >= 97 && c <= 122 {
                    result.push(c - 32)
                } else {
                    result.push(c)
                }
            } else {
                if head_uppercase && j == 0 {
                    if c >= 97 && c <= 122 {
                        result.push(c - 32)
                    } else {
                        result.push(c)
                    }
                } else {
                    result.push(c)
                }
            }
        }
        j += 1;
    }
    String::from_utf8_lossy(result.as_slice()).to_string()
}

// 表名转实体名
// @param table_name: 表名
// @param table_prefix: 前缀名
pub fn table_name_to_entity_name(table_name: String, table_prefix: String) -> String {
    let option = table_name.strip_prefix(table_prefix.as_str()).unwrap();
    underline_to_hump(option.to_string(), true)
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let end = underline_to_hump("bb_Vv_Ddd_sss_1".to_string(), true);
        print!("{}", end);
    }

    #[test]
    fn test_table_name_to_entity_name() {
        let string = table_name_to_entity_name("t_dick_bank".to_string(), "t_".to_string());
        let string2 = table_name_to_entity_name("t_dick".to_string(), "t_".to_string());
        println!("{}", string);
        println!("{}", string2);
    }

}