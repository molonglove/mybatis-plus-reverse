use std::fs;
use serde::{Deserialize, Serialize};
use crate::database::{DatabaseConfig};
use crate::TableName;

// 应用配置
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ApplicationConfig {
    // 项目路径
    pub project_path: Option<String>,
    // 基础包路径
    pub package_path: String,
    // 是否跳过打印
    pub pass_print: bool,
    // 数据库配置
    pub database: DatabaseConfig,
    // 实体类配置
    pub entity: EntityConfig,
    // 注释作者配置
    pub author: Option<String>,
}

// 实体类配置
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EntityConfig {
    // 主键方式
    pub primary_key_type: String,
    // 实体类路径
    pub entity_path: Option<String>,
    // 是否增加lombok注解
    pub use_lombok: bool,
    // 时间格式
    pub time_format: Option<String>,
    // 时区配置
    pub time_zone: Option<String>
}

impl ApplicationConfig {
    pub fn new(config_path: &str, file_path: &str, author: &str) -> Self {
        let result = fs::read_to_string(config_path).expect("文件不存在");
        let mut config: ApplicationConfig = serde_json::from_str(result.as_str()).unwrap();
        config.author = Some(author.to_string());
        config.project_path = Some(file_path.to_string());
        return config
    }

    pub fn load_fields(&self) -> Vec<TableName> {
        self.database.load_fields().unwrap_or(vec![])
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn test() {}

}



